# Complete C++ example

This repository gives an example of what a pipeline might look like in real life with a C++ project.

## Source code

The source code is a trivial _Hello World_ in C++, see [src/hello.cpp](src/hello.cpp) and [tests/tests.cpp](tests/tests.cpp) defining a test (googletest). The project must be built with CMake
and googletest is required.
```sh
cmake -B build -S .
cmake --build build
./build/runTests
```

## Gitlab runners

The gitlab-ci configuration is written in the file
[.gitlab-ci.yml](.gitlab-ci.yml). See the results of the pipeline
[here](https://gitlab.inria.fr/gitlabci_gallery/pipelines/complete_cpp_example/-/pipelines).

We have set three gitlab-ci [runners](https://docs.gitlab.com/runner/install/),
[Linux shared runners](https://ci.inria.fr/doc/page/gitlab/) with docker executor, one macOS Catalina and one Windows 10 with shell executor that we setup on [ci.inria.fr](https://ci.inria.fr/) (see the [documentation](https://ci.inria.fr/doc/page/web_portal_tutorial/)).

### Requirements on macOS and Windows machines

On macOS install [Brew](https://brew.sh/) then
```sh
brew install cmake git googletest
```

On Windows install [MSYS2](https://www.msys2.org/) then in a UCRT64 shell:
```sh
pacman -S bash base-devel cmake gcc git gtest
```

## Gitlab-CI pipeline

We present a classical pipeline.

- The tests are triggered on several systems (here Linux with Docker, macOS Catalina and Windows 10) and in parallel if runners are available at the same time.
- In the Linux case we execute a sequence of jobs:
  - *docker_buildenv*: builds the docker image with requirements from this [.gitlab/Dockerfile](.gitlab/Dockerfile) and pushed on the Deploy -> [Container Registry](https://gitlab.inria.fr/gitlabci_gallery/pipelines/complete_cpp_example/container_registry) with the tag *gitlabci*.
  - *test_linux*: builds the project with cmake and runs the tests with ctest in the [.gitlab/test.sh](.gitlab/test.sh) script. Some unitary tests and coverage reports are also uploaded as artifacts (see the details in the next section).
  - Inside the job *test_linux* we also call a script [.gitlab/lint.sh](.gitlab/lint.sh) which performs an analysis of the source code with linters such as cppcheck, valgrind. The static and dynamic analysis reports are uploaded on the [Inria's Sonarqube](https://sonarqube.inria.fr/) service, see the [complete_cpp_example sonarqube project](https://sonarqube.inria.fr/sonarqube/dashboard?id=gitlabci_gallery_pipelines_complete_cpp_example_AZJMSngJsbMNg1jXgmw7). We have used the [Gitlab integration](https://docs.sonarsource.com/sonarqube/9.9/devops-platform-integration/gitlab-integration/#importing-your-gitlab-projects-into-sonarqube) feature of SonarQube. Notice that if the SonarQube's Quality Gate fails then the *test_linux* job will fail. In addition [pull request analysis](https://docs.sonarsource.com/sonarqube/9.9/analyzing-source-code/pull-request-analysis/) can be enabled: add the *sonarqubebot* user as __Reporter__ member to your project, this will allow SonarQube to add comments on the new code (issues, vulnerabilities, code smells) during Merge Request pipelines (do not forget to use the environment variable GIT_DEPTH: "0" to get git blame information), see [MR 4](https://gitlab.inria.fr/gitlabci_gallery/pipelines/complete_cpp_example/-/merge_requests/4).
  - *pages*: builds the documentation and upload it on the pages server, see Deploy -> Pages -> [Access pages](https://gitlabci_gallery.gitlabpages.inria.fr/pipelines/complete_cpp_example/). This is done only on the main branch.
  - *docker_main*: builds a docker image from this [Dockerfile](Dockerfile) with the project installed and ready to be used by end users. This is done only on the main branch. This image is pushed on the Deploy -> [Container Registry](https://gitlab.inria.fr/gitlabci_gallery/pipelines/complete_cpp_example/container_registry).
  - *package_tag*: makes packages using the script [.gitlab/package.sh](.gitlab/package.sh) when new tags of name vX.Y (e.g. v0.1, v1.2 etc) are pushed. It creates a .tar.gz source tarball and a .deb debian package and upload it on the package registry, see Deploy -> [Package Registry](https://gitlab.inria.fr/gitlabci_gallery/pipelines/complete_cpp_example/-/packages). Finally it also creates a gitlab's release visible in Deploy -> [Releases](https://gitlab.inria.fr/gitlabci_gallery/pipelines/complete_cpp_example/-/releases).

Notice also the [badges](https://docs.gitlab.com/ee/user/project/badges.html) that have been set on the [main page of the project](https://gitlab.inria.fr/gitlabci_gallery/pipelines/complete_cpp_example), see pipeline, coverage, latest release, sonarqube, pages (e.g. the documentation).

## Remarks on some gitlab-ci jobs tricks used here

Some specific keywords are used in the [.gitlab-ci.yml](.gitlab-ci.yml) script. We give some explanations.

Notice that we do not use any technique to factorize the pipeline code, see keywords [extends](https://docs.gitlab.com/ee/ci/yaml/#extends) and [include](https://docs.gitlab.com/ee/ci/yaml/#include) which should be used as good practices. This is wanted to ease the reading and to get a clear view of all notions and keywords we rely on in this example.

- `interruptible: true` is useful for example when we push new commits several times on the same branch. In this situation we are only interested in the last pipeline. The previous ones can be cancelled.
- `artifacts` allows to transmit files generated by a job to other ones lying in following stages, useful when several dependent jobs may not be executed on the same runner (it is the case when using shared runners which are docker executors). In addition, the *test_linux* job generates two reports, one unitary test report `test.xml` and one coverage report `coverage.xml`. They are used for sonarqube and also as [artifacts's report](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html) to get a view of unitary tests status in the panel __Tests__ of each pipeline and to see the coverage (see badge coverage, see coverage on modified lines in merge requests, long-time statistics in Analyze -> Repository analytics). We use `expire_in: 24h` to remove artifacts from gitlab after 24 hours (the default expiration time is 30 days) knowing that by default the latest pipeline's artifacts are kept on each branch (see Settings -> CI/CD -> Artifacts).
- `cache` allows to transmit files between subsequent jobs which are executed on the same runner (the case here for macOS and windows which are custom runners). It is not useful here because we only have one job on macOS and windows but if we plan to add jobs later depending on them we will be able to get the generated files using the same `cache` keyword (with the same key and `policy: pull` possibly).
- `when: always` used in artifacts parameters means we want to get artifacts of the job even if the script fails. It is useful for debugging, recall that on docker runners we do not always have access to the machines.
- By default jobs lying in subsequent stages download __all__ artifacts from previous jobs. It is less ressource consuming and error prone to be precise about what is needed by the job. This can be done with the `need:` keyword. Moreover, if artifacts are not needed at all then `dependencies: []` can be used to tell this job will not download any artifact from previous jobs.
- `rules:` allows to limit the job creation only for some conditions:
  - `if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH` means the job appears only for pushes on the main branch.
  - `if: $CI_COMMIT_TAG =~ /^v[0-9]\.[0-9]$/` means the job is created only when pushing new tags following the naming convention v1.0, v2.6, etc.
  - complex rules can be generated using several `- if `, an example here of job triggered if not a "schedule" job, if push on branches whose name begin by "ci-", if push on the main branch but only for a certain project namespace (useful when using forks), if new merge request and for branches whose name do not begin by "notest-":
  ```
    - if: ($CI_PIPELINE_SOURCE == "schedule" )
      when: never
    - if: ($CI_COMMIT_BRANCH =~ /^ci-.*$/)
    - if: ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH) &&
          (($CI_PROJECT_ROOT_NAMESPACE == "solverstack"   ) ||
           ($CI_PROJECT_ROOT_NAMESPACE == $CI_PROJECT_NAME))
    - if: (($CI_PIPELINE_SOURCE == "merge_request_event") &&
           ($CI_MERGE_REQUEST_SOURCE_BRANCH_NAME !~ /^notest-.*$/))
  ```
- `changes` is used here to re-build the docker image (see *docker_buildenv* job) used in tests only if the Dockerfile changes. Notice also the feature `when: manual` that could be used to trigger jobs only if users press a button in the gitlab's pipelines interface.
