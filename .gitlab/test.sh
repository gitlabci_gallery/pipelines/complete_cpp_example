#!/bin/bash

set -ex

# specific compilation flags on linux for linters
if [[ "$SYSTEM" == "linux" ]]; then
    if [[ "$CXX" == "g++" ]]; then
        FLAGS="-O0 -g -Wall -fPIC -fno-inline --coverage"
        SCAN_BUILD="scan-build -v -plist --intercept-first --analyze-headers -o analyzer_reports "
    fi
fi

if [ -d "build/" ]; then
  rm build/ -rf;
fi

# configure the cmake project
${SCAN_BUILD}cmake -B build --install-prefix $PWD/install \
  -DCMAKE_CXX_FLAGS="$FLAGS" \
  -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
  -DCMAKE_VERBOSE_MAKEFILE=ON

# build the project (compile)
${SCAN_BUILD}cmake --build build | tee build.log

# install the project (copy some files) in the installation directory (see install-prefix)
cmake --install build

# execute the tests
ctest --test-dir build --output-on-failure --no-compress-output --output-junit $PWD/test.xml

# apply linters (static and dynamic analysis)
if [[ "$SYSTEM" == "linux" ]]; then
    if [[ "$CXX" == "g++" ]]; then
        .gitlab/lint.sh
    fi
fi
