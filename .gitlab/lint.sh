#!/bin/bash

set -ex

# GNU coverage
gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml --root $PWD

# cppcheck
cppcheck -v --max-configs=1 --language=c++ --platform=unix64 --enable=all --xml --xml-version=2 --suppress=missingIncludeSystem --project=build/compile_commands.json 2> cppcheck.xml

# valgrind
valgrind --xml=yes --xml-file=valgrind.xml --memcheck:leak-check=full --show-reachable=yes "./build/hello"

# sonarqube (https://docs.sonarsource.com/sonarqube/9.9/analyzing-source-code/scanners/sonarscanner/)
# create the sonarqube config file
cat > sonar-project.properties << EOF
sonar.projectKey=gitlabci_gallery_pipelines_complete_cpp_example_AZJMSngJsbMNg1jXgmw7
sonar.qualitygate.wait=true

sonar.links.homepage=$CI_PROJECT_URL
sonar.links.scm=$CI_REPOSITORY_URL
sonar.links.ci=$CI_PROJECT_URL/pipelines
sonar.links.issue=$CI_PROJECT_URL/issues

sonar.projectDescription=C++ example for gitlab-ci pipelines
sonar.projectVersion=0.4

sonar.scm.disabled=false
sonar.scm.provider=git
sonar.scm.exclusions.disabled=true

sonar.sourceEncoding=UTF-8
sonar.sources=.
sonar.exclusions=build/CMakeFiles/**,**/*.xml
sonar.cxx.file.suffixes=.hpp,.cpp
sonar.cxx.jsonCompilationDatabase=build/compile_commands.json
sonar.cxx.errorRecoveryEnabled=true
sonar.cxx.gcc.encoding=UTF-8
sonar.cxx.gcc.regex=(?<file>.*):(?<line>[0-9]+):[0-9]+:\\\x20warning:\\\x20(?<message>.*)\\\x20\\\[(?<id>.*)\\\]
sonar.cxx.gcc.reportPaths=build.log
sonar.cxx.xunit.reportPaths=test.xml
sonar.cxx.cobertura.reportPaths=coverage.xml
sonar.cxx.cppcheck.reportPaths=cppcheck.xml
sonar.cxx.clangsa.reportPaths=analyzer_reports/*/*.plist
sonar.cxx.valgrind.reportPaths=valgrind.xml
EOF
echo "====== sonar-project.properties ============"
cat sonar-project.properties
echo "============================================"

# run sonar analysis + publish on sonarqube
#sonar-scanner -X > sonar.log
sonar-scanner
