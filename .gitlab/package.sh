#!/bin/bash

set -ex

cmake -B build
cmake --build build --target package_source
cmake --build build --target package

# delete the release if already exists
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --request DELETE "https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/releases/$CI_COMMIT_TAG" | jq

# upload packages on the package registry
RELEASE_NUM=`echo $CI_COMMIT_TAG | sed -e "s#v##"`
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ./build/Hello-$RELEASE_NUM.tar.gz "https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/packages/generic/release/$RELEASE_NUM/Hello-$RELEASE_NUM.tar.gz"
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ./build/Hello-$RELEASE_NUM-Linux.deb "https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/packages/generic/release/$RELEASE_NUM/Hello-$RELEASE_NUM-Linux.deb"

# release creation
CMD=`echo curl --header \"Content-Type: application/json\" \
               --header \"JOB-TOKEN: $CI_JOB_TOKEN\" \
               --data \'{ \"name\": \"$CI_COMMIT_TAG\", \"tag_name\": \"$CI_COMMIT_TAG\", \
                          \"assets\": { \"links\": [{ \"name\": \"Hello-$RELEASE_NUM.tar.gz\", \"url\": \"https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/packages/generic/release/$RELEASE_NUM/Hello-$RELEASE_NUM.tar.gz\" }, \
                                                    { \"name\": \"Hello-$RELEASE_NUM-Linux.deb\", \"url\": \"https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/packages/generic/release/$RELEASE_NUM/Hello-$RELEASE_NUM-Linux.deb\" }] } \
                        }\' \
               --request POST \"https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/releases\"`
eval $CMD | jq
