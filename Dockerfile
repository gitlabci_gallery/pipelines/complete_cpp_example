FROM alpine
RUN apk add bash make gcc g++ cmake gtest-dev

COPY ./src/ /tmp/src/
COPY ./tests/ /tmp/tests/
COPY ./CMakeLists.txt /tmp/CMakeLists.txt
RUN cd /tmp && \
    cmake -B build && \
    cmake --build build && \
    cmake --install build && \
    rm /tmp/* -r

# Create a group and user
RUN addgroup -S gitlab && adduser -S gitlab -G gitlab

# default user
USER gitlab

# default working directory
WORKDIR /home/gitlab

SHELL ["/bin/bash", "-c"]
