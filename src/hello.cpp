#include <hello.hpp>

int main(int argc, char **argv) {
    HelloWorld::PrintHelloWorld();
    HelloWorld::MakeBug();
    return EXIT_SUCCESS;
}