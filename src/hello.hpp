#ifndef HELLO_HPP
#define HELLO_HPP

#include <iostream>

struct HelloWorld
{
    void static PrintHelloWorld()
    {
        std::cout << "Hello World!\n";
    }
    void static MakeBug()
    {
        int bugd = 1;
        int* bugbuf = new int[2];
        bugbuf[0] = bugd;
        delete[] bugbuf;
    }

};

#endif // HELLO_HPP